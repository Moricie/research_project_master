import numpy as np
import time
import json
import matplotlib.pyplot as plt

# Calculate intersection over union for two boxes.
# Taken from:
'''https://medium.com/@venuktan/vectorized-intersection-
over-union-iou-in-numpy-and-tensor-flow-4fa16231b63d'''

def Iou(bboxes1, bboxes2):
    x11, y11, x12, y12 = np.split(bboxes1, 4, axis=0)
    x21, y21, x22, y22 = np.split(bboxes2, 4, axis=0)  

    xA = np.maximum(x11, np.transpose(x21))
    yA = np.maximum(y11, np.transpose(y21))
    xB = np.minimum(x12, np.transpose(x22))
    yB = np.minimum(y12, np.transpose(y22))        
    
    interArea = np.maximum((xB - xA + 1), 0) * np.maximum((yB - yA + 1), 0)        
    
    boxAArea = (x12 - x11 + 1) * (y12 - y11 + 1)
    boxBArea = (x22 - x21 + 1) * (y22 - y21 + 1)        
    
    iou = interArea / (boxAArea + np.transpose(boxBArea) - interArea)   

    return iou    




# Reads the detection and groundtruth data json files
# thresh = confidence threshold
def evaluate_detections(thresh):    
    if(thresh == 0):
        thresh = 0.05

    groundtruth = open("groundtruth.json", "r")

    ssd_detection = open("ssd_detection.json", "r")



    gt_dict = json.load(groundtruth)

    ssd_dict = json.load(ssd_detection)

    # Counts of positives
    true_positives = 0
    false_positives = 0

    # Average number of positives per frame
    tp_average = 0
    fp_average = 0

    # Number of bounding boxes in groundtruth
    groundtruth_box_count = 0

    # Count of frames with detections or groundtruth
    # May be below the 200 frames
    frame_count = 0

    i = 0
    #0.0015625
    while i < len(gt_dict[0])-1:
        #print(gt_dict[0]["Frame" + str(i)])
        frame_gt = gt_dict[0]["Frame" + str(i)]
        box_count = 1
        while box_count < frame_gt["box_count"]:
            box_coord0 = np.asarray(frame_gt["Box" + 
            str(box_count)][1:-1].split(), dtype=np.float64, order='C')
            box_count += 1

            # Don't consider bounding boxes below this size
            # (Values from 0 to 1 normalized from
            # the pixel size of the image)
            if((box_coord0[2] - box_coord0[0]) >= 0.004\
                 and (box_coord0[3] - box_coord0[1]) >= 0.015):
                #print(frame0["Box" + str(box_count)])
                
                #print(len(frame0))
                
                frame_ssd = ssd_dict[0]["Frame" + str(i)]
                detect_count = 0
                true_positives_per_frame = 0
                false_positives_per_frame = 0
                found_one = False

                # Search for true detections by comparing
                # the detections with the ground truth
                for detection in frame_ssd["Detections"]:
                    if(float(detection['Confidence']) > thresh):
                        box_coord1 = np.asarray(detection['Box'][1:-1]
                        .split(), dtype=np.float64, order='C')
                        detect_count += 1
                        iou = Iou(box_coord0, box_coord1)
                        #print(iou)
                        # Prevents accidentally counting
                        # the same detection as true twice
                        if(iou > 0.5 and found_one == False):
                            #print("Foundone: " + str(detect_count))
                            true_positives += 1
                            true_positives_per_frame += 1
                            found_one = True

                    
                    
                

                #print(tp_average)
                #print(detect_count, box_count)
                groundtruth_box_count += frame_gt["box_count"]

        # Only update the average if there are detections and groundtruth.
        if(frame_gt["box_count"] > 0 and detect_count > 0):
            #print(detect_count)
            tp_average += true_positives_per_frame\
                 / (detect_count * frame_gt["box_count"])
            fp_average += false_positives_per_frame\
                 / (detect_count * frame_gt["box_count"])
            

        if(frame_gt["box_count"] > 0):
            frame_count +=1

        false_positives += detect_count - true_positives_per_frame
        false_positives_per_frame = detect_count - true_positives_per_frame

        i += 1

    print("\n")
    print(groundtruth_box_count)
    print("Threshold:", str(thresh))
    print("True Positives:", str(true_positives))
    print("False Positives:", str(false_positives))
    tpr = 0 
    fpr = 0

    # Only update the detection rates of true positives vs
    # false positives if there are detections at all
    if(true_positives > 0 or false_positives > 0):
        tpr = (true_positives / (true_positives+false_positives)) * 100
        fpr = (false_positives / (true_positives+false_positives)) * 100
        
    print("True Positive rate:", str(tpr))
    print("False Positive rate:", str(fpr))
    
    print("True Positives average:", str(true_positives / (i+1)))
    print("False Positives average:", str(false_positives / (i+1)))
    print("Found rate: ", str(true_positives / groundtruth_box_count))

    print("Frame Count:", str(frame_count))

    return (tpr, fpr, groundtruth_box_count, true_positives
    , false_positives, tp_average, fp_average, frame_count)




tprs = np.zeros([11])
fprs = np.zeros([11])
groundtruth_box_counts = 0
true_positives_arr = np.zeros([11])
false_positives_arr = np.zeros([11])
tpa = np.zeros([11])
fpa = np.zeros([11])
frame_count = np.zeros([11])

# Evaluate detections for the different threshold values
# and plots different plots from them
for it in range(0, 10):
    tprs[it], fprs[it], groundtruth_box_counts, true_positives_arr[it], \
        false_positives_arr[it], tpa[it], fpa[it], \
            frame_count[it] = evaluate_detections(0.1 * it)
    if(frame_count[it] == 0):
        frame_count[it] = 1
axis_threshold = np.array([0.01, 0.1, 0.2, 0.3, 0.4,\
     0.5, 0.6, 0.7, 0.8, 0.9, 1.0])


plt.rc('font', family='serif')
plt.rc('xtick', labelsize='medium')
plt.rc('ytick', labelsize='medium')

# Shows the percentage of true detections from all detections
fig0 = plt.figure(figsize=(4, 3))
ax0 = fig0.add_subplot(1, 1, 1)

ax0.plot(axis_threshold, tprs)
ax0.set_title("Share of true detections")
ax0.set_ylabel("tp / (fp + tp) %")
ax0.set_xlabel("Confidence Threshold")


# Shows the percentage of false detections from all detections
fig1 = plt.figure(figsize=(4, 3))
ax1 = fig1.add_subplot(1, 1, 1)

ax1.plot(axis_threshold, fprs)
ax1.set_title("Share of false detections")
ax1.set_ylabel("fp / (tp + fp) %")
ax1.set_xlabel("Confidence Threshold")


# Shows how many of the groundtruth bounding boxes
# were found by the ssd in percent.
fig2 = plt.figure(figsize=(4, 3))
ax2 = fig2.add_subplot(1, 1, 1)

ax2.plot(axis_threshold, (true_positives_arr / groundtruth_box_counts) * 100)
ax2.set_title("Percentile tp of the groundtruth")
ax2.set_ylabel("tp / groundtruth_boxes")
ax2.set_xlabel("Confidence Threshold")


# Shows the percentage of true positives left
# from all true detections with each threshold value
fig3 = plt.figure(figsize=(4, 3))
ax3 = fig3.add_subplot(1, 1, 1)

ax3.plot(axis_threshold, (true_positives_arr / true_positives_arr[0]) * 100)
ax3.set_title("Percentile tp vs no threshold tp")
ax3.set_ylabel("tpk / tp0")
ax3.set_xlabel("Confidence Threshold")


# Plots the true positive rate per frame against
# the false positives per frame
fig4 = plt.figure(figsize=(4, 3))
ax4 = fig4.add_subplot(1, 1, 1)

ax4.plot((false_positives_arr / frame_count), \
    ((true_positives_arr / frame_count)\
     / (groundtruth_box_counts / frame_count)) * 100)
ax4.set_title("True positive rate per frame vs. false positives per frame")
ax4.set_ylabel("tpr")
ax4.set_xlabel("fp")

plt.show()