#!/usr/bin/env python3

import subprocess ;
import time ;


from AirSimClient import CarClient, CarControls, ImageRequest ;
from AirSimClient import AirSimImageType, AirSimClientBase ;
import pprint ;
import os ;

import airsim ;
import cv2 ;


import numpy as np
import os
import six.moves.urllib as urllib
import sys
import tarfile
import tensorflow as tf
import zipfile
import pathlib

from collections import defaultdict
from io import StringIO


from PIL import Image
from IPython.display import display


from object_detection.utils import ops as utils_ops
from object_detection.utils import label_map_util
from object_detection.utils import visualization_utils as vis_util

from scipy import ndimage

# patch tf1 into `utils.ops`
utils_ops.tf = tf.compat.v1

# Patch the location of gfile
tf.gfile = tf.io.gfile

# For measuring the inference time.
import time ;

# Check available GPU devices.
#print("The following GPU devices are available: 
#  %s" % tf.test.gpu_device_name()) ;

print(tf.version)

# List of the strings that is used to add correct label for each box.
PATH_TO_LABELS = 'models/research/object_detection/data/mscoco_label_map.pbtxt'
category_index = (label_map_util
.create_category_index_from_labelmap(PATH_TO_LABELS, use_display_name=True))

f_count = 0

def run_inference_for_single_image(model, image):
  image = np.asarray(image)
  # The input needs to be a tensor, convert it using `tf.convert_to_tensor`.
  input_tensor = tf.convert_to_tensor(image)
  # The model expects a batch of images, so add an axis with `tf.newaxis`.
  input_tensor = input_tensor[tf.newaxis,...]

  # Run inference
  output_dict = model(input_tensor)

  # All outputs are batches tensors.
  # Convert to numpy arrays, and take index [0] to remove the batch dimension.
  # We're only interested in the first num_detections.
  num_detections = int(output_dict.pop('num_detections'))
  output_dict = {key:value[0, :num_detections].numpy() 
                 for key,value in output_dict.items()}
  output_dict['num_detections'] = num_detections

  # detection_classes should be ints.
  output_dict['detection_classes'] = (output_dict['detection_classes']
  .astype(np.int64))
   
  # Handle models with masks:
  if 'detection_masks' in output_dict:
    # Reframe the the bbox mask to the image size.
    detection_masks_reframed = utils_ops.reframe_box_masks_to_image_masks(
              output_dict['detection_masks'], output_dict['detection_boxes'],
               image.shape[0], image.shape[1])
    detection_masks_reframed = tf.cast(detection_masks_reframed > 0.5,
                                       tf.uint8)
    output_dict['detection_masks_reframed'] = detection_masks_reframed.numpy() 

  return output_dict ;


def show_inference(model, image_path):
  # the array based representation of the image
  # will be used later in order to prepare the
  # result image with boxes and labels on it.
  image_np = np.array(Image.open(image_path))
  # Actual detection.
  output_dict = run_inference_for_single_image(model, image_np)

  try:
    os.stat('./eval_txts') ;
  except:
    os.mkdir('./eval_txts') ;

  f = open("./ssd_detection.json", "a+") ;

  global f_count ;
  f.write("\"Frame" + str(f_count) + "\": {") ;
  f.write("\"Detections\": [")
  f_count += 1 ;

  i = 0
  no_det = 0
  # Write the detections to the file
# Classes: {1 : person, 3 : car, 10 : traffic_light}
  while i < len(output_dict['detection_boxes']):
    if(output_dict['detection_classes'][i] == 1):
      if(no_det > 0):
        f.write(",\n")
      f.write("{\n")
      f.write("\"Box\":\"" + 
      str(output_dict['detection_boxes'][i]) + "\",\n") ;
      f.write("\"Confidence\":\"" + 
      str(output_dict['detection_scores'][i]) + "\"\n}\n") ;
      no_det += 1
    i += 1

  f.write("],\n\"no_detections\":" + str(no_det) + "\n},\n")
  f.close() ;

  # Visualization of the results of a detection.
  vis_util.visualize_boxes_and_labels_on_image_array(
      image_np,
      output_dict['detection_boxes'],
      output_dict['detection_classes'],
      output_dict['detection_scores'],
      category_index,
      instance_masks=output_dict.get('detection_masks_reframed', None),
      use_normalized_coordinates=True,
      line_thickness=8)


  display_image(Image.fromarray(image_np))


def load_model(model_name):
  base_url = 'http://download.tensorflow.org/models/object_detection/'
  model_file = model_name + '.tar.gz'
  model_dir = tf.keras.utils.get_file(
    fname=model_name, 
    origin=base_url + model_file,
    untar=True)

  model_dir = pathlib.Path(model_dir)/"saved_model"
  model = tf.compat.v2.saved_model.load(str(model_dir), None)
  model = model.signatures['serving_default']

  return model
  

def display_image(image):

  
  import matplotlib
  matplotlib.use("TkAgg")
  import matplotlib.pyplot as plt
  #print(matplotlib.get_backend())

  plt.close()
  fig = plt.figure(figsize=(20, 15)) ;
  plt.grid(False) ;
  plt.ion() ;
  plt.imshow(image) ;
  plt.show() ;
  plt.pause(.001) ;



def load_img(path):
  img = tf.io.read_file(path) ;
  img = tf.image.decode_jpeg(img, channels=3) ;
  return img ;


def extract_mask(image, rgb_value):

  mask = image ;
  mask1 = (mask[:,:,0] == rgb_value[0]) ;
  mask2 = (mask[:,:,1] == rgb_value[1]) ;
  mask3 = (mask[:,:,2] == rgb_value[2]) ;
  mask[:,:,0] = mask1 ;
  mask[:,:,1] = mask2 ;
  mask[:,:,2] = mask3 ;
  mask = np.sum(mask, axis=2) ;
  mask = mask * 255 ;
  return mask ;



# ssd_mobilenet_v1_coco_2017_11_17

# alternative ssd_mobilenet_v2_coco_2018_03_29
# unfortunately seems to have a built in threshold

model_name = 'ssd_mobilenet_v1_coco_2017_11_17'
model = load_model(model_name)
print(model.inputs)



f = open("./groundtruth.json", "w+") ;
f.write("[\n{")
f.close()

f = open("./ssd_detection.json", "w+") ;
f.write("[\n{")
f.close()

# Create image directories if they don't exist already
try:
    os.stat('./pictures') ;
except:
    os.mkdir('./pictures') ;


try:
    os.stat('./pictures/depth') ;
except:
    os.mkdir('./pictures/depth') ;

try:
    os.stat('./pictures/seg') ;
except:
    os.mkdir('./pictures/seg') ;

try:
    os.stat('./pictures/mask_street') ;
except:
    os.mkdir('./pictures/mask_street') ;
    
try:
    os.stat('./pictures/mask_traffic_light') ;
except:
    os.mkdir('./pictures/mask_traffic_light') ;

try:
    os.stat('./pictures/mask_pedestrian') ;
except:
    os.mkdir('./pictures/mask_pedestrian') ;

try:
    os.stat('./pictures/col') ;
except:
    os.mkdir('./pictures/col') ;


# Start the environment
subprocess.call([r'run.bat']) ;
# Wait for it to load up
time.sleep(10) ;

# Connect to the AirSim simulator 
client = airsim.CarClient() ;
client.confirmConnection() ;
client.enableApiControl(True) ;
car_controls = airsim.CarControls() ;


# Set colors for segmentation of street and side walk to (238, 129, 126)
success = client.simSetSegmentationObjectID("landscape_1", 46, True) ;

# Set color for non traffic lights to (109, 153, 222)
# and keep traffic lights at (210, 59, 240)
success = client.simSetSegmentationObjectID("[\w]*light[\w]*1", 47, True) ;

# Set color for cars to
success = client.simSetSegmentationObjectID("[\w]*Veh[\w]*", 60, True)

# Set color for pedestrians to
success = client.simSetSegmentationObjectID("[\w]*ped[\w]*", 49, True)
success = client.simSetSegmentationObjectID("[\w]*person[\w]*", 49, True)

# Teleport vehicle to starting point of the image collection road
pose = client.simGetVehiclePose() ;
pose.position = pose.position + airsim.Vector3r(16.6666, 26.84457, 0) ;
pose.orientation = (pose.orientation +
airsim.Quaternionr(0.0, 0.0, 0.28174763917, -0.19647222757)) ;
client.simSetVehiclePose(pose, True) ;

# Wait for cars to make space
time.sleep(7) ;

# Go forward and keep the speed after four seconds
car_controls.throttle = 0.5 ;
car_controls.steering = 0 ;
client.setCarControls(car_controls) ;
time.sleep(2) ;


# Main Loop for image extraction and object detection.

# Index for filenames
t = [1] ;

while True:
   
  # Pause simulation for calculation
  client.simPause(True) ;
  
  # Get Lidar Data
  #lidar = client.getLidarData();
  #print(lidar)

  # Retrieve segmentation image from the car camera
  # to extract the street and traffic light mask
  responses_seg = client.simGetImages([ImageRequest(0, AirSimImageType
  .Segmentation, False, False)]) ;
  response_seg = responses_seg[0] ;
  seg = np.frombuffer(response_seg.image_data_uint8, dtype=np.uint8) ;
  seg = seg.reshape(response_seg.height, response_seg.width, 3) ;

  # save picture
  cv2.imwrite('pictures/{}/seg{}.png'.format("seg", t[0]), seg) ;
  
  # Calculate Mask for the street depending on the chosen rgb value below
  rgb_for_mask0 = [238, 129, 162] ;
  street_mask = extract_mask(np.copy(seg[:,:,:3]), rgb_for_mask0) ;
  cv2.imwrite('pictures/{}/mask_street{}.png'
  .format("mask_street", t[0]), np.float32(street_mask)) ;


  # Calculate Mask for the traffic lights depending on the rgb value below
  rgb_for_mask1 = [210, 59, 240] ;
  traffic_light_mask = extract_mask(np.copy(seg[:,:,:3]), rgb_for_mask1) ;
  cv2.imwrite('pictures/{}/mask_traffic_light{}.png'
  .format("mask_traffic_light", t[0]), np.float32(traffic_light_mask)) ;
  

  # Calculate Mask for the pedestrians depending on the rgb value below
  rgb_for_mask2 = [215, 142, 98] ;
  pedestrian_mask = extract_mask(np.copy(seg[:,:,:3]), rgb_for_mask2) ;
  cv2.imwrite('pictures/{}/mask_pedestrian{}.png'
  .format("mask_pedestrian", t[0]), np.float32(pedestrian_mask)) ;
  # Label objects
  labeled_image, num_features = ndimage.label(pedestrian_mask) ;
  # Find the location of all objects
  objs = ndimage.find_objects(labeled_image) ;

  f = open("./groundtruth.json", "a+") ;
  f.write("\"Frame" + str(f_count) + "\": {\n") ;
  box_count = 0
  for ob in objs:
    box_count += 1
    if(box_count > 1):
      f.write(",")
    f.write("\"Box" + str(box_count) + "\":\"[ " +
    str(ob[0].start / 480) + " " +
    str(ob[1].start / 640) + " " +
    str(ob[0].stop / 480) + " " +
    str(ob[1].stop / 640) + " ]\"\n") ;
    #f.write("Class: " + str(1) + "\n\n") ;

  if(box_count > 0):
      f.write(",")  
  f.write("\"box_count\":" + str(box_count))
  f.write("},\n")
  f.close() ;

  # Retrieve depth images from the car camera
  responses_depth = client.simGetImages([ImageRequest(0,
  AirSimImageType.DepthVis, True, False)]) ;
  response_depth = responses_depth[0] ;
  # convert to grayscale
  depth = np.array(response_depth.image_data_float, dtype=np.float32) ;
  depth = depth.reshape(response_depth.height, response_depth.width) ;
  depth = np.array(depth * 255, dtype=np.uint8) ;
  #print(depth)
  # save pic
  cv2.imwrite('pictures/{}/depth{}.png'.format("depth", t[0]), depth) ;


  # Retrieve RGB camera images from the car camera
  responses_col = client.simGetImages([ImageRequest(0,
  AirSimImageType.Scene, False, False)])   ;
  response_col = responses_col[0] ;
  col = np.frombuffer(response_col.image_data_uint8, dtype=np.uint8) ;
  col = col.reshape(response_col.height, response_col.width, 3) ;

  col_path = 'pictures\\{}\\col{}.png'.format("col", t[0]) ;

  cv2.imwrite(col_path, col) ;

  # Retrieve RGB image as float for object detection
  responses_det = client.simGetImages([ImageRequest(0,
  AirSimImageType.Scene, False, False)]) ;
  response_det = responses_det[0] ;
  det = np.frombuffer(response_det.image_data_uint8, dtype=np.uint8) ;
  det = det.reshape(1, response_det.height, response_det.width, 3) ;
  det = tf.compat.v1.constant(det, tf.float32) ;
  det /= 255 ;
  det = det[:, :, :, :3] ;


  # object detection
  
  img = load_img(col_path) ;

  start_time = time.time() ;
  show_inference(model, col_path)
  end_time = time.time() ;

  print("Inference time: ", end_time-start_time) ;


  t[0] += 1 ;

  
  client.simPause(False) ;
  time.sleep(0.15) ;

  if(t[0] == 201):

    f = open("./groundtruth.json", "a+") ;
    f.write("\"End\":0\n}\n]")
    f.close()

    f = open("./ssd_detection.json", "a+") ;
    f.write("\"End\":0\n}\n]")
    f.close()

    break

