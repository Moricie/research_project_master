The script is written for the City Environment (CityEnviron) of the latest windows prerelease version of AirSim 1.2.2.
It might behave differently in different versions of this AirSim environment.

Tested with Tensorflow 2.1

The settings.json file has to be placed in /<path_to_Documents>/Documents/AirSim.

The files AirSimClient.py and object_detection_loop_local.py need to be placed in the extracted city environment folder that contains the run.bat file which is the outermost extracted folder called "CityEnviron".

object_detection_loop_local.py is the file that needs to be executed with python and will create a folder "pictures" that will contain the rgba, segmentation and depth images and the street and traffic light masks and run the object detection loop.

Helpful links:

-Lidar: https://github.com/microsoft/AirSim/blob/master/docs/lidar.md

-Airsim APIs: https://github.com/microsoft/AirSim/blob/master/docs/apis.md

-Uses Tensorflow Object Detection API: https://github.com/tensorflow/models/tree/master/research/object_detection

-Used ssd: ssd_mobilenet_v1_coco_2017_11_17